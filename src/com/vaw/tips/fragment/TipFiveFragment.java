package com.vaw.tips.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.vaw.selfhelp.R;

public class TipFiveFragment extends SherlockFragment {

	ViewPager pager;
	Button next, prev;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.tipfive_fragment, container, false);
		pager = (ViewPager)getSherlockActivity().findViewById(R.id.pager);
		prev = (Button)view.findViewById(R.id.previousButtonS5);
		next = (Button)view.findViewById(R.id.nextButtonS5);
		prev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pager.setCurrentItem(3, true);
			}
		});
		next.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getSherlockActivity(), "New tips are coming. This is all for now.", Toast.LENGTH_LONG ).show();
			}
		});
		return view;
	}

}
