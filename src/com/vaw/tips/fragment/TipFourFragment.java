package com.vaw.tips.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;
import com.vaw.selfhelp.R;

public class TipFourFragment extends SherlockFragment {

	ViewPager pager;
	Button prev, next;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.tipfour_fragment, container, false);
		pager = (ViewPager)getSherlockActivity().findViewById(R.id.pager);
		prev = (Button)view.findViewById(R.id.previousButtonS4);
		next = (Button)view.findViewById(R.id.nextButtonS4);
		prev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pager.setCurrentItem(2, true);
			}
		});
		next.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			pager.setCurrentItem(4, true);
			}
		});
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

}
