package com.vaw.tips.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;
import com.vaw.selfhelp.R;

public class TipTwoFragment extends SherlockFragment {

	Button previousButtonS2, nextButtonS2;
	ViewPager mViewPager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater
				.inflate(R.layout.tiptwo_fragment, container, false);
		mViewPager = (ViewPager) getSherlockActivity().findViewById(R.id.pager);
		previousButtonS2 = (Button) view.findViewById(R.id.previousButtonS2);
		nextButtonS2 = (Button) view.findViewById(R.id.nextButtonS2);
		previousButtonS2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mViewPager.setCurrentItem(0, true);
			}
		});
		nextButtonS2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mViewPager.setCurrentItem(2, true);
			}
		});
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

}
