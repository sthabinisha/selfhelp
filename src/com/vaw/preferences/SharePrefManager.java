package com.vaw.preferences;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
 
public class SharePrefManager {
    // Shared Preferences
    SharedPreferences pref;
 
    // Editor for Shared preferences
    Editor editor;
 
    // Context
    Context _context;
 
    // Shared pref mode
    int PRIVATE_MODE = 0;
 
    // Sharedpref file name
    private static final String PREF_NAME = "selfhelppref";

    public static final String KEY_NUMBER1 = "number1";
    public static final String KEY_NUMBER2 = "number2";
    public static final String KEY_NUMBER3 = "number3";
    public static final String KEY_NUMBER4 = "number4";
    public static final String KEY_NUMBER5 = "number5";
    public static final String KEY_TIMER = "timer";
    public static final String KEY_ENUMBER = "emergencynumber";
    public static final String KEY_STATE = "state";
    public static final String KEY_HELPMESSAGE = "helpmessage";
    
    public SharePrefManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
 
   public void setValue(String key, String value){
	   editor.putString(key, value);
	   editor.commit();
   }
   
   public List<String> getAllValue(){
	   List<String> list = new ArrayList<String>();
	   if(pref.getString(KEY_NUMBER1 , "").length()>2)
		   list.add(pref.getString(KEY_NUMBER1 , ""));
	   if(pref.getString(KEY_NUMBER2 , "").length()>2)
		   list.add(pref.getString(KEY_NUMBER2 , ""));
	   if(pref.getString(KEY_NUMBER3 , "").length()>2)
		   list.add(pref.getString(KEY_NUMBER3 , ""));
	   if(pref.getString(KEY_NUMBER4 , "").length()>2)
		   list.add(pref.getString(KEY_NUMBER4 , ""));
	   if(pref.getString(KEY_NUMBER5 , "").length()>2)
		   list.add(pref.getString(KEY_NUMBER5 , ""));
	   
	   return list;
   }
   
   public void setState(int number){
	   editor.putInt(KEY_STATE, number);
	   editor.commit();
   }
   
   public int getState(){
	   return pref.getInt(KEY_STATE, 1);
   }
   
   public boolean isvalueavailiable(){
	   if(pref.getString(KEY_NUMBER1, null)==null && pref.getString(KEY_NUMBER2, null)==null)
		   return false;
	   else
		   return true;
   }
   
   public void setHelpMessage(String help){
	   editor.putString(KEY_HELPMESSAGE, help);
	   editor.commit();
   }
   
   public void setEmergencyNumber(String no){
	   editor.putString(KEY_ENUMBER, no);
	   editor.commit();
   }
   
   public String getHelpMessage(){
	   return pref.getString(KEY_HELPMESSAGE, "Help! I\'m in trouble!");
   }
   
   public String getValue(String key){
	   return pref.getString(key, "");
   }
   
   public String getEmergencyNumber(){
	   return pref.getString(KEY_ENUMBER, "100");
   }
   
   public void removeValue(String key){
	   editor.putString(key, null);
	   editor.commit();
   }
   
   public void setTimer(String timer){
	   long toLong = Long.parseLong(timer)*1000;
	   editor.putLong(KEY_TIMER, toLong);
	   editor.commit();
   }
   
   public long getTimer(){
	   return pref.getLong(KEY_TIMER, 5000);
   }
   
   public void setcount(int number){
	   editor.putInt("numberoftouch", number);
	   editor.commit();
   }
   
   public int getcount(){
	   return pref.getInt("numberoftouch", 0);
   }
   
   public void settime(long value){
	   editor.putLong("lasttime", value);
	   editor.commit();
   }
   
   public long getlasttime(){
	   return pref.getLong("lasttime", 0);
   }
 
}