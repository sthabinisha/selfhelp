package com.vaw.libraries;

import java.util.List;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.vaw.gpstracker.GPSTracker;
import com.vaw.preferences.SharePrefManager;
import com.vaw.selfhelp.MainActivity;

public class SmsController {

	SharePrefManager SM;
	Context _context;

	public SmsController(Context c) {
		this._context = c;
		SM = new SharePrefManager(_context);
	}

	public boolean send() {
		List<String> list = SM.getAllValue();
		if (list.size() == 0) {
			Log.i("list of name", "null");
			Intent i = new Intent(_context, MainActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			_context.startActivity(i);
			return false;

		}

		GPSTracker gps = new GPSTracker(_context);
		if (gps.canGetLocation()) {
			String latitude = String.valueOf(gps.getLatitude());
			String longitude = String.valueOf(gps.getLongitude());

			String sms = SM.getHelpMessage()
					+ " My location:\nhttps://maps.google.com/maps?q="
					+ String.valueOf(latitude) + ","
					+ String.valueOf(longitude);
			
			Log.i("SMS Message", sms);

			for (String string : list) {
				String SENT = "SMS_SENT";
				String DELIVERED = "SMS_DELIVERED";
				PendingIntent sentPI = PendingIntent.getBroadcast(_context, 0,
						new Intent(SENT), 0);
				PendingIntent deliveredPI = PendingIntent.getBroadcast(
						_context, 0, new Intent(DELIVERED), 0);
				android.telephony.SmsManager smsmanager = android.telephony.SmsManager.getDefault();
				smsmanager.sendTextMessage(string, null, sms, sentPI,
						deliveredPI);
				Log.i("sms sent to", string);
			}
		} else {
			gps.showSettingsAlert();
		}

		return true;
	}
}
