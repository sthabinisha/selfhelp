package com.vaw.selfhelp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vaw.preferences.SharePrefManager;
import com.vaw.utils.Utils;

public class PhoneActivity extends Activity {

	ImageView backButton;
	Button callPhone;

	SharePrefManager SM;
	TextView enumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.emergency_call);
		SM = new SharePrefManager(getApplicationContext());
		backButton = (ImageView) findViewById(R.id.phoneBack);
		enumber = (TextView) findViewById(R.id.enumber);
		enumber.setText(SM.getEmergencyNumber());

		callPhone = (Button) findViewById(R.id.callPhone);

		if (!SM.getEmergencyNumber().equalsIgnoreCase("100")) {
				callPhone.setText("Call Emergency Number");
		}

		Utils.TypeFaceButton(callPhone, getAssets());

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		callPhone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				emergencyCall();
			}
		});

	}

	public void emergencyCall() {
		String number = "tel:" + SM.getEmergencyNumber();
		Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
		try {
			startActivity(callIntent);
		} catch (Exception e) {
			Toast.makeText(PhoneActivity.this,
					"Sorry calling failed, please try again",
					Toast.LENGTH_SHORT).show();
		}

	}
	

}
