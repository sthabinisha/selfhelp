package com.vaw.selfhelp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ReportSubmission extends Activity {





	EditText messageToSend;
	Button cancelPost, sendPost;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
			if(messageToSend.getText().toString().length()>0){
				final AlertDialog alert = new AlertDialog.Builder(ReportSubmission.this).create();
				alert.setTitle("Message body has text !");
				alert.setMessage("Your message body contains some text, are you sure you want to exit.");
				alert.setButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						finish();
					}
				});
				alert.setButton2("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						alert.cancel();
					}
				});
				
				alert.show();
			}	
		}
			
		return super.onKeyDown(keyCode, event);
	}


	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_submission);
		cancelPost = (Button)findViewById(R.id.cancelPost);
		sendPost = (Button)findViewById(R.id.sendPost);
		messageToSend = (EditText)findViewById(R.id.messageToSend);
		
		cancelPost.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(messageToSend.getText().toString().length()>0){
					final AlertDialog alert = new AlertDialog.Builder(ReportSubmission.this).create();
					alert.setTitle("Message body has text !");
					alert.setMessage("Your message body contains some text, are you sure you want to exit.");
					alert.setButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							// TODO Auto-generated method stub
							finish();
						}
					});
					alert.setButton2("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							alert.cancel();
						}
					});
					
					alert.show();
				}else{
					finish();
				}
				
			}
		});
		
		sendPost.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(messageToSend.getText().toString().length()>0){
					String message = "VAW "+messageToSend.getText().toString();
					String phoneNumber = "5002";
					try{
						SmsManager smsManager = SmsManager.getDefault();
						smsManager.sendTextMessage(phoneNumber, null, message, null, null);
						Toast.makeText(getApplicationContext(), "Your message has been sent", Toast.LENGTH_SHORT).show();
						finish();
					}catch(Exception e){
						Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
						e.printStackTrace();
					}
				}else{
					Toast.makeText(getApplicationContext(), "The message should have a body", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	
	
}
