package com.vaw.selfhelp;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

public class Receiver extends Service {

	Notification notification;
	private static final int NOTIFICATION_ID = 0;
	NotificationManager manager;
	PendingIntent toOpen;
	Intent intent;
	private BroadcastReceiver POWER_BUTTON = new Powerbuttonrecceiver();

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		registerReceiver(POWER_BUTTON, filter);
		startNotify();
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(POWER_BUTTON);
		dismissNotification();
		super.onDestroy();
	}

	public void startNotify() {
		manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		int icon = R.drawable.ic_launcher_drop;
		CharSequence tickerText = "Safety mode on!";
		CharSequence tickerContent = "Safety mode is now on. You can press your power button in case of emergencies. Tap to turn this feature off";
		intent = new Intent(Receiver.this, SMSOptions.class);
		toOpen = PendingIntent.getActivity(getApplicationContext(), 0, intent,
				0);

		notification = new NotificationCompat.Builder(getApplicationContext())
				.setContentTitle(tickerText).setContentText(tickerContent)
				.setSmallIcon(icon).setOngoing(true).setContentIntent(toOpen)
				.build();
		manager.notify(NOTIFICATION_ID, notification);
	}

	public void dismissNotification() {
		manager.cancel(NOTIFICATION_ID);
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		// TODO Auto-generated method stub
		if (Build.VERSION.SDK_INT <= 19) {
			Intent restartService = new Intent(getApplicationContext(),
					this.getClass());
			restartService.setPackage(getPackageName());
			PendingIntent restartServicePI = PendingIntent.getService(
					getApplicationContext(), 1, restartService,
					PendingIntent.FLAG_ONE_SHOT);
			AlarmManager alarmService = (AlarmManager) getApplicationContext()
					.getSystemService(Context.ALARM_SERVICE);
			alarmService.set(AlarmManager.ELAPSED_REALTIME,
					SystemClock.elapsedRealtime() + 1000, restartServicePI);
		}
	}

}