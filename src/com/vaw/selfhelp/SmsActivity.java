package com.vaw.selfhelp;

import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaw.gpstracker.GPSTracker;
import com.vaw.libraries.SmsController;
import com.vaw.libraries.Smsdeliverbroadcast;
import com.vaw.libraries.Smssendbroadcast;
import com.vaw.preferences.SharePrefManager;
import com.vaw.utils.Utils;

public class SmsActivity extends Activity {

	ImageView backButton;
	Button sendSMSButton;


	double longitude, latitude;
	LinearLayout listofnumbers;
	SharePrefManager SM;

	GPSTracker gps;

	private BroadcastReceiver sendBroadcastReceiver;
	private BroadcastReceiver deliveryBroadcastReceiver;

	String SENT = "SMS_SENT";
	String DELIVERED = "SMS_DELIVERED";
	String numberName = null;
	Smssendbroadcast smssendbroadcast;
	Smsdeliverbroadcast smsdeliverybroadcast;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quick_sms);

		
		smssendbroadcast = new Smssendbroadcast();
		smsdeliverybroadcast = new Smsdeliverbroadcast();
		registerReceiver(smssendbroadcast, new IntentFilter(DELIVERED));
		registerReceiver(smsdeliverybroadcast , new IntentFilter(SENT));
		
		SM = new SharePrefManager(getApplicationContext());

		List<String> list = SM.getAllValue();
		backButton = (ImageView) findViewById(R.id.smsBack);
		sendSMSButton = (Button) findViewById(R.id.sendSMS);
		Utils.TypeFaceButton(sendSMSButton, getAssets());

		listofnumbers = (LinearLayout) findViewById(R.id.listofnumber);
		listofnumbers.removeAllViews();

		for (String string : list) {
			TextView textview = new TextView(getApplicationContext());
			textview.setText("+977-" + string);
			textview.setTextColor(getResources().getColor(R.color.white));
			textview.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources()
					.getDimension(R.dimen.qsllTvTextSize));
			textview.setGravity(Gravity.CENTER);
			listofnumbers.addView(textview);
		}



		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		sendSMSButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (SM.getTimer() == 0) {
					new SmsController(SmsActivity.this).send();
				} else {
					Intent bb = new Intent(SmsActivity.this, SureSMS.class);
					bb.putExtra("timer", SM.getTimer());
					startActivity(bb);
				}
			}

		});

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		try {
			unregisterReceiver(sendBroadcastReceiver);
			unregisterReceiver(deliveryBroadcastReceiver);
		} catch (IllegalArgumentException i) {
			i.printStackTrace();
		}

		super.onStop();

	}


}
