package com.vaw.selfhelp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.vaw.tips.fragment.TipFiveFragment;
import com.vaw.tips.fragment.TipFourFragment;
import com.vaw.tips.fragment.TipOneFragment;
import com.vaw.tips.fragment.TipThreeFragment;
import com.vaw.tips.fragment.TipTwoFragment;

public class TipsSlider extends SherlockFragmentActivity {

	private FragmentPagerAdapter adapter;
	private ViewPager pager;
	ImageView tipsBackSwipe;

	public static final String[] CONTENT = new String[] { "Tip 1", "Tip 2",
			"Tip 3", "Tip 4", "Tip 5" };

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.safety_tips_swipey);
		adapter = new SwipeyTips(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);
		tipsBackSwipe = (ImageView) findViewById(R.id.tipsBackSwipe);
		tipsBackSwipe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	class SwipeyTips extends FragmentPagerAdapter {

		public SwipeyTips(FragmentManager fm) {
			super(fm);
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new TipOneFragment();
				break;
			case 1:
				fragment = new TipTwoFragment();
				break;
			case 2:
				fragment = new TipThreeFragment();
				break;
			case 3:
				fragment = new TipFourFragment();
				break;
			case 4:
				fragment = new TipFiveFragment();
				break;
			default:
				break;
			}

			return fragment;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 5;
		}

	}

}
