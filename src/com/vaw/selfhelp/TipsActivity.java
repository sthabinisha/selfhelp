package com.vaw.selfhelp;

import com.vaw.utils.Utils;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TipsActivity extends Activity{

	ImageView backButton;
	Button nextButton, previousButton;
	TextView counter, stTVQuestion, stTVAnswer;
	int counterClick = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.safety_tips);
		
		backButton = (ImageView)findViewById(R.id.tipsBack);
		nextButton = (Button)findViewById(R.id.nextButton);
		previousButton = (Button)findViewById(R.id.previousButton);
		counter = (TextView)findViewById(R.id.counter);
		stTVAnswer = (TextView)findViewById(R.id.stTVAnswer);
		stTVQuestion = (TextView)findViewById(R.id.stTVQuesiton);
		
		Utils.TypeFaceButton(nextButton, getAssets());
		Utils.TypeFaceButton(previousButton, getAssets());
		
		final String[] questions = {getResources().getString(R.string.tipQuestion1), getResources().getString(R.string.tipQuestion2), getResources().getString(R.string.tipQuestion3), getResources().getString(R.string.tipQuestion4), getResources().getString(R.string.tipQuestion5)};
		final String[] answers = {getResources().getString(R.string.tipAnswer1), getResources().getString(R.string.tipAnswer2),getResources().getString(R.string.tipAnswer3),getResources().getString(R.string.tipAnswer4),getResources().getString(R.string.tipAnswer5)};
		StringBuilder builder = new StringBuilder();
		for(String s : questions) {
		    builder.append(s);
		}
		
		
		Log.i("QUESTIONS",builder.toString());
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		nextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(counterClick<5){
					counterClick++;
				}else{
					Toast.makeText(getApplicationContext(), "That's all for now.", Toast.LENGTH_SHORT).show();
				}
				for(int i = 0; i< counterClick; i++ ){
					stTVQuestion.setText(questions[i]);
					stTVAnswer.setText(answers[i]);
					counter.setText(String.valueOf(counterClick)+"/5");
					Log.i("I ko Value", String.valueOf(i));
				}
			}
		});
		
		previousButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(counterClick>1){
				counterClick= counterClick-1;
				counter.setText(String.valueOf(counterClick)+"/5");
				int toArr = counterClick-1;
				stTVQuestion.setText(questions[toArr]);
				stTVAnswer.setText(answers[toArr]);
				}else{
					Toast.makeText(getApplicationContext(), "No previous.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
	}

	
	
}
