package com.vaw.selfhelp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vaw.preferences.SharePrefManager;
import com.vaw.utils.Utils;

public class SMSOptions extends Activity {

	EditText Number1, Number2, Number3, Number4, Number5;
	TextView remove2, remove3, remove4, remove5;
	ImageView intent1, intent2, intent3, intent4, intent5;
	LinearLayout phoneNo1, phoneNo2, phoneNo3, phoneNo4, phoneNo5;
	TextView addMe;
	LinearLayout phoneNumberCollection;
	
	CompoundButton cb;

	EditText eNum;

	LinearLayout editMessage;

	EditText helpMessage;

	Button saveState;

	EditText delayTimer;
	TextView secTV;

	static final int PICK_CONTACT = 1;

	ImageView smsSettingsBack;

	SharePrefManager SM;
	
	int state;
	Intent serviceIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub11
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quick_sms_settings);
		SM = new SharePrefManager(getApplicationContext());

		smsSettingsBack = (ImageView) findViewById(R.id.smsSettingsBack);

		eNum = (EditText) findViewById(R.id.eNumber);
		String emergency = SM.getEmergencyNumber();
		eNum.setText(emergency);
		
		cb = (CompoundButton) findViewById(R.id.activate);

		Number1 = (EditText) findViewById(R.id.Number1);
		Number2 = (EditText) findViewById(R.id.Number2);
		Number3 = (EditText) findViewById(R.id.Number3);
		Number4 = (EditText) findViewById(R.id.Number4);
		Number5 = (EditText) findViewById(R.id.Number5);

		intent1 = (ImageView) findViewById(R.id.intent1);
		intent2 = (ImageView) findViewById(R.id.intent2);
		intent3 = (ImageView) findViewById(R.id.intent3);
		intent4 = (ImageView) findViewById(R.id.intent4);
		intent5 = (ImageView) findViewById(R.id.intent5);

		addMe = (TextView) findViewById(R.id.addMe);

		remove2 = (TextView) findViewById(R.id.remove2);
		remove3 = (TextView) findViewById(R.id.remove3);
		remove4 = (TextView) findViewById(R.id.remove4);
		remove5 = (TextView) findViewById(R.id.remove5);

		phoneNo1 = (LinearLayout) findViewById(R.id.phoneNo1);
		phoneNo2 = (LinearLayout) findViewById(R.id.phoneNo2);
		phoneNo3 = (LinearLayout) findViewById(R.id.phoneNo3);
		phoneNo4 = (LinearLayout) findViewById(R.id.phoneNo4);
		phoneNo5 = (LinearLayout) findViewById(R.id.phoneNo5);

		phoneNumberCollection = (LinearLayout) findViewById(R.id.phoneNumberCollection);

		int childCount = phoneNumberCollection.getChildCount();
		Log.i("CHILD COUNT", String.valueOf(childCount));
		int count = 0;
		for (int i = 0; i < childCount; i++) {
			if (phoneNumberCollection.getChildAt(i).getVisibility() == View.VISIBLE) {
				count++;
			}
		}
		Log.i("VISIBLE CHILD", String.valueOf(count));

		secTV = (TextView) findViewById(R.id.secTV);
		Utils.TypeFaceLight(secTV, getAssets());

		delayTimer = (EditText) findViewById(R.id.delayTimer);
		String timerTime = String.valueOf(SM.getTimer() / 1000);
		delayTimer.setText(timerTime);
		Utils.TypeFaceET(delayTimer, getAssets());
		// editMessage = (LinearLayout)findViewById(R.id.editMessage);

		saveState = (Button) findViewById(R.id.saveNumbers);
		Utils.TypeFaceButton(saveState, getAssets());

		helpMessage = (EditText) findViewById(R.id.helpMessage);
		String message = SM.getHelpMessage();
		helpMessage.setText(message);

		if (!SM.getValue(SM.KEY_NUMBER1).equalsIgnoreCase("")) {
			Number1.setText(SM.getValue(SM.KEY_NUMBER1));
		}
		if (!SM.getValue(SM.KEY_NUMBER2).equalsIgnoreCase("")) {
			Number2.setText(SM.getValue(SM.KEY_NUMBER2));
			addLayout(phoneNo2);
		}
		if (!SM.getValue(SM.KEY_NUMBER3).equalsIgnoreCase("")) {
			Number3.setText(SM.getValue(SM.KEY_NUMBER3));
			addLayout(phoneNo3);
		}
		if (!SM.getValue(SM.KEY_NUMBER4).equalsIgnoreCase("")) {
			Number4.setText(SM.getValue(SM.KEY_NUMBER4));
			addLayout(phoneNo4);
		}
		if (!SM.getValue(SM.KEY_NUMBER5).equalsIgnoreCase("")) {
			Number5.setText(SM.getValue(SM.KEY_NUMBER5));
			addLayout(phoneNo5);
		}
		
		state = SM.getState();
		
		if(state == 0){
			cb.setChecked(false);
		}else{
			cb.setChecked(true);
		}
		
		serviceIntent = new Intent(SMSOptions.this, Receiver.class);
		
		cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked == true){
					//start service
					startService(serviceIntent);
					SM.setState(1);
				}else{
					//end service
					stopService(serviceIntent);
					SM.setState(0);
				}
			}
		});

		remove2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				phoneNo2.setVisibility(View.GONE);
				int count = getVisibileCount(phoneNumberCollection);
				if (count < 5) {
					addMe.setVisibility(View.VISIBLE);
				}
				if (Number2.getText().toString().length() > 0)
					Number2.setText("");
				SM.removeValue(SM.KEY_NUMBER2);
			}
		});
		remove3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				phoneNo3.setVisibility(View.GONE);
				int count = getVisibileCount(phoneNumberCollection);
				if (count < 5) {
					addMe.setVisibility(View.VISIBLE);
				}
				if (Number3.getText().toString().length() > 0)
					Number3.setText("");
				SM.removeValue(SM.KEY_NUMBER3);
			}
		});
		remove4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				phoneNo4.setVisibility(View.GONE);
				int count = getVisibileCount(phoneNumberCollection);
				if (count < 5) {
					addMe.setVisibility(View.VISIBLE);
				}
				if (Number4.getText().toString().length() > 0)
					Number4.setText("");
				SM.removeValue(SM.KEY_NUMBER4);
			}
		});
		remove5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				phoneNo5.setVisibility(View.GONE);
				int count = getVisibileCount(phoneNumberCollection);
				if (count < 5) {
					addMe.setVisibility(View.VISIBLE);
				}
				if (Number5.getText().toString().length() > 0)
					Number5.setText("");
				SM.removeValue(SM.KEY_NUMBER5);
			}
		});

		addMe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				int childern = phoneNumberCollection.getChildCount();
				for (int i = 0; i < childern; i++) {
					if (phoneNumberCollection.getChildAt(i).getVisibility() == View.GONE) {
						phoneNumberCollection.getChildAt(i).setVisibility(
								View.VISIBLE);
						break;
					}
				}
				int count = getVisibileCount(phoneNumberCollection);
				if (count == 5) {
					addMe.setVisibility(View.GONE);
				} else if (count < 5) {
					addMe.setVisibility(View.VISIBLE);
				}
			}
		});

		intent1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent add = new Intent(Intent.ACTION_PICK,
						ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(add, 0);
			}
		});

		intent2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent add = new Intent(Intent.ACTION_PICK,
						ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(add, 1);
			}
		});

		intent3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent add = new Intent(Intent.ACTION_PICK,
						ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(add, 2);
			}
		});

		intent4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent add = new Intent(Intent.ACTION_PICK,
						ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(add, 3);
			}
		});

		intent5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent add = new Intent(Intent.ACTION_PICK,
						ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(add, 4);
			}
		});

		smsSettingsBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		saveState.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (Number1.getText().toString().length() < 2) {
					Toast.makeText(getApplicationContext(),
							"Please set number to save.", Toast.LENGTH_LONG)
							.show();
				} else {
					String number1 = Number1.getText().toString();
					String number2 = Number2.getText().toString();
					String number3 = Number3.getText().toString();
					String number4 = Number4.getText().toString();
					String number5 = Number5.getText().toString();
					String message = helpMessage.getText().toString();
					String timer = delayTimer.getText().toString();
					String emergency = eNum.getText().toString();
					SM.setValue(SM.KEY_NUMBER1, number1);
					SM.setValue(SM.KEY_NUMBER2, number2);
					SM.setValue(SM.KEY_NUMBER3, number3);
					SM.setValue(SM.KEY_NUMBER4, number4);
					SM.setValue(SM.KEY_NUMBER5, number5);
					SM.setValue(SM.KEY_HELPMESSAGE, message);
					SM.setEmergencyNumber(emergency);
					SM.setTimer(timer);
					Toast.makeText(getApplicationContext(),
							"Your settings has been saved", Toast.LENGTH_LONG)
							.show();
				}
			}
		});

	}

	public void addLayout(LinearLayout layout) {
		layout.setVisibility(View.VISIBLE);
	}

	public void removeLayout(LinearLayout layout) {
		layout.setVisibility(View.GONE);
	}

	public void removeTextView(TextView tv) {
		tv.setVisibility(View.GONE);
	}

	public void addTextView(TextView tv) {
		tv.setVisibility(View.VISIBLE);
	}

	private int getVisibileCount(LinearLayout layout) {
		int childCount = layout.getChildCount();
		int count = 0;
		for (int i = 0; i < childCount; i++) {
			if (layout.getChildAt(i).getVisibility() == View.VISIBLE) {
				count++;
			}
		}
		return count;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 0:
			if (resultCode == Activity.RESULT_OK) {

				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {

					String id = c
							.getString(c
									.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

					String hasPhone = c
							.getString(c
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

					if (hasPhone.equalsIgnoreCase("1")) {
						Cursor phones = getContentResolver()
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = " + id, null, null);
						phones.moveToFirst();
						String cNumber = phones.getString(phones
								.getColumnIndex("data1"));
						if (cNumber.startsWith("+977")) {
							cNumber = cNumber.substring(4);
						} else if (cNumber.startsWith("977")) {
							cNumber = cNumber.substring(3);
						} else if (cNumber.startsWith("0")) {
							cNumber = cNumber.substring(1);
						} else if (cNumber.startsWith("+977-")) {
							cNumber = cNumber.substring(5);
						} else if (cNumber.startsWith("977-")) {
							cNumber = cNumber.substring(4);
						}

						Number1.setText(cNumber);
					}
				}
			}
			break;

		case 1:
			if (resultCode == Activity.RESULT_OK) {

				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {

					String id = c
							.getString(c
									.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

					String hasPhone = c
							.getString(c
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

					if (hasPhone.equalsIgnoreCase("1")) {
						Cursor phones = getContentResolver()
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = " + id, null, null);
						phones.moveToFirst();
						String cNumber = phones.getString(phones
								.getColumnIndex("data1"));
						if (cNumber.startsWith("+977")) {
							cNumber = cNumber.substring(4);
						} else if (cNumber.startsWith("977")) {
							cNumber = cNumber.substring(3);
						} else if (cNumber.startsWith("0")) {
							cNumber = cNumber.substring(1);
						} else if (cNumber.startsWith("+977-")) {
							cNumber = cNumber.substring(5);
						} else if (cNumber.startsWith("977-")) {
							cNumber = cNumber.substring(4);
						}

						Number2.setText(cNumber);
						phoneNo2.setVisibility(View.VISIBLE);
					}
				}
			}
			break;
		case 2:
			if (resultCode == Activity.RESULT_OK) {

				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {

					String id = c
							.getString(c
									.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

					String hasPhone = c
							.getString(c
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

					if (hasPhone.equalsIgnoreCase("1")) {
						Cursor phones = getContentResolver()
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = " + id, null, null);
						phones.moveToFirst();
						String cNumber = phones.getString(phones
								.getColumnIndex("data1"));
						if (cNumber.startsWith("+977")) {
							cNumber = cNumber.substring(4);
						} else if (cNumber.startsWith("977")) {
							cNumber = cNumber.substring(3);
						} else if (cNumber.startsWith("0")) {
							cNumber = cNumber.substring(1);
						} else if (cNumber.startsWith("+977-")) {
							cNumber = cNumber.substring(5);
						} else if (cNumber.startsWith("977-")) {
							cNumber = cNumber.substring(4);
						}

						Number3.setText(cNumber);
						phoneNo3.setVisibility(View.VISIBLE);
					}
				}
			}
			break;

		case 3:
			if (resultCode == Activity.RESULT_OK) {

				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {

					String id = c
							.getString(c
									.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

					String hasPhone = c
							.getString(c
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

					if (hasPhone.equalsIgnoreCase("1")) {
						Cursor phones = getContentResolver()
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = " + id, null, null);
						phones.moveToFirst();
						String cNumber = phones.getString(phones
								.getColumnIndex("data1"));
						if (cNumber.startsWith("+977")) {
							cNumber = cNumber.substring(4);
						} else if (cNumber.startsWith("977")) {
							cNumber = cNumber.substring(3);
						} else if (cNumber.startsWith("0")) {
							cNumber = cNumber.substring(1);
						} else if (cNumber.startsWith("+977-")) {
							cNumber = cNumber.substring(5);
						} else if (cNumber.startsWith("977-")) {
							cNumber = cNumber.substring(4);
						}

						Number4.setText(cNumber);
						phoneNo4.setVisibility(View.VISIBLE);
					}
				}
			}
			break;
		case 4:
			if (resultCode == Activity.RESULT_OK) {

				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {

					String id = c
							.getString(c
									.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

					String hasPhone = c
							.getString(c
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

					if (hasPhone.equalsIgnoreCase("1")) {
						Cursor phones = getContentResolver()
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = " + id, null, null);
						phones.moveToFirst();
						String cNumber = phones.getString(phones
								.getColumnIndex("data1"));
						if (cNumber.startsWith("+977")) {
							cNumber = cNumber.substring(4);
						} else if (cNumber.startsWith("977")) {
							cNumber = cNumber.substring(3);
						} else if (cNumber.startsWith("0")) {
							cNumber = cNumber.substring(1);
						} else if (cNumber.startsWith("+977-")) {
							cNumber = cNumber.substring(5);
						} else if (cNumber.startsWith("977-")) {
							cNumber = cNumber.substring(4);
						}

						Number5.setText(cNumber);
						phoneNo5.setVisibility(View.VISIBLE);
					}
				}
			}
			break;
		}
	}

}
