package com.vaw.selfhelp;

import com.vaw.preferences.SharePrefManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HelpMessage extends Activity{
	
	Button cancel, post;
	SharePrefManager SM;
	EditText helpText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help_message);
		SM = new SharePrefManager(getApplicationContext());
		helpText = (EditText)findViewById(R.id.helpText);
		cancel = (Button)findViewById(R.id.cancelHelp);
		post = (Button)findViewById(R.id.saveHelp);
		
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		post.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String helpTXT = helpText.getText().toString();
				if(helpTXT.equalsIgnoreCase("")){
					Toast.makeText(getApplicationContext(), "Please set a help text message !", Toast.LENGTH_LONG).show();
				}else{
					SM.setHelpMessage(helpTXT);
					Toast.makeText(getApplicationContext(), "Your new help text message has been set !", Toast.LENGTH_LONG).show();
					finish();
				}
			}
		});
	}
		
}
