package com.vaw.selfhelp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class ConfirmationDialog extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.confirmationdialog);
		
		findViewById(R.id.yes).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				try{
					
				}catch(Exception e){
					Toast.makeText(ConfirmationDialog.this, "Sorry calling failed, please try again", Toast.LENGTH_SHORT).show();
				}finally{
					finish();
				}
				
				
			}
		});
		
		findViewById(R.id.no).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
	}

}
