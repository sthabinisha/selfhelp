package com.vaw.selfhelp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.vaw.libraries.SmsController;
import com.vaw.preferences.SharePrefManager;

public class Powerbuttonrecceiver extends BroadcastReceiver {
	private static final long INCREMENT_STALL = 1000;
	private SharePrefManager SM;
	private int count;
	NotificationManager manager;
	Notification notification;
	PendingIntent toOpen;
	Intent notificationIntent;
	private static int SERVER_DATA_RECEIVED = 1;
	CharSequence notiText;
	CharSequence contentTitle;
	CharSequence contentText;

	@Override
	public void onReceive(Context context, Intent intent) {
		SM = new SharePrefManager(context);
		count = SM.getcount();
		Log.i("count", String.valueOf(count));

		if (count == 0 || count > 5) {
			SM.setcount(1);
			SM.settime(System.currentTimeMillis());
		} else {
			count++;
			long lasttime = SM.getlasttime();
			if (System.currentTimeMillis() - lasttime < INCREMENT_STALL) {
				if (count == 3) {
					Toast.makeText(context, "pressed more then 3",
							Toast.LENGTH_LONG).show();

					notiText = "Panic SMS has been sent";
					contentTitle = "SelfHelp";
					contentText = "Panic SMS has been sent to all specified number(s)";

					if (SM.isvalueavailiable()){
						new SmsController(context).send();
					}
					else {
						notiText = "Panic SMS couldn't be sent ";
						contentTitle = "SelfHelp";
						contentText = "Save numbers in settings to send SMS";
					}

					manager = (NotificationManager) context
							.getSystemService(Context.NOTIFICATION_SERVICE);
					int icon = R.drawable.ic_launcher_drop;
					notificationIntent = new Intent(context, MainActivity.class);
					toOpen = PendingIntent.getActivity(context, 0,
							notificationIntent, 0);

					notification = new NotificationCompat.Builder(context)
							.setContentTitle(contentTitle)
							.setTicker(contentText).setContentText(notiText)
							.setSmallIcon(icon)
//							.setContentIntent(toOpen)
							.build();
					manager.notify(SERVER_DATA_RECEIVED, notification);

					Vibrator vibrator = (Vibrator) context
							.getSystemService(Context.VIBRATOR_SERVICE);
					vibrator.vibrate(500);
					SM.setcount(0);

				} else {
					SM.setcount(count);
					SM.settime(System.currentTimeMillis());
				}
			} else {
				SM.setcount(0);
			}
		}
	}

}
