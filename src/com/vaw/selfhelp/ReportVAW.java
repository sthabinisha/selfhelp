package com.vaw.selfhelp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vaw.utils.Utils;

public class ReportVAW extends Activity{

	ImageView reportSMS, reportPhone, backButton;
	TextView reportSMSText, reportPhoneText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reportvaw);
		
		reportSMS = (ImageView)findViewById(R.id.reportSMS);
		reportSMSText = (TextView)findViewById(R.id.reportSMSText);
		reportPhone = (ImageView)findViewById(R.id.reportPhone);
		reportPhoneText = (TextView)findViewById(R.id.reportPhoneText);
		backButton = (ImageView)findViewById(R.id.backButtonREVAW);
		
		Utils.TypeFaceReport(reportSMSText, getAssets());
		Utils.TypeFaceReport(reportPhoneText, getAssets());
		
		reportPhone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String number = "tel:015008727";
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
				try{
					startActivity(callIntent);
				}catch(Exception e){
					Toast.makeText(ReportVAW.this, "Sorry calling failed, please try again", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		reportSMS.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent reporting = new Intent(ReportVAW.this, ReportSubmission.class);
		    	startActivity(reporting);
			}
		});
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();	
			}
		});
	}
	
}
