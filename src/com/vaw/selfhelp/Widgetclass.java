package com.vaw.selfhelp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.vaw.libraries.SmsController;
import com.vaw.preferences.SharePrefManager;

public class Widgetclass extends AppWidgetProvider {
	// our actions for our buttons
	public static String ACTION_WIDGET_MAKECALL = "ActionReceiverEmergencycall";
	public static String ACTION_WIDGET_SENDSMS = "ActionReceiverEmergencySMS";
	SharePrefManager SM;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),R.layout.widgets);

		Intent callintent = new Intent(context, Widgetclass.class);
		callintent.setAction(ACTION_WIDGET_MAKECALL);
		PendingIntent callPendingIntent = PendingIntent.getBroadcast(context,0, callintent, 0);
		remoteViews.setOnClickPendingIntent(R.id.makephonecall,callPendingIntent);

		Intent smsintent = new Intent(context, Widgetclass.class);
		smsintent.setAction(ACTION_WIDGET_SENDSMS);
		PendingIntent smsPendingIntent = PendingIntent.getBroadcast(context, 0,smsintent, 0);
		remoteViews.setOnClickPendingIntent(R.id.sendsms, smsPendingIntent);

		appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
	}

	@Override
	public void onReceive(final Context context, Intent intent) {
		SM = new SharePrefManager(context);
		
		if (intent.getAction().equals(ACTION_WIDGET_MAKECALL)) {
//			*** Make Phone Call **********
			Intent callintent = new Intent(Intent.ACTION_CALL);
			callintent.setData(Uri.parse("tel:"+SM.getEmergencyNumber()));
			callintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(callintent);
			
		}else if (intent.getAction().equals(ACTION_WIDGET_SENDSMS)) {
			if(SM.isvalueavailiable()){
				if (SM.getTimer() == 0) {
					new SmsController(context).send();
				} else {
					Intent bb = new Intent(context, SureSMS.class);
					bb.putExtra("timer", SM.getTimer());
					bb.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(bb);
				}
			}else{
				Toast.makeText(context, "Please set phone number from setting before using this feature", Toast.LENGTH_LONG).show();
			}
			
			
			
		} else {
			super.onReceive(context, intent);
		}
	}
		
}
