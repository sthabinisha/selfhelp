package com.vaw.selfhelp;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.vaw.libraries.SmsController;
import com.vaw.preferences.SharePrefManager;
import com.vaw.utils.Utils;

public class SureSMS extends SherlockActivity {

	LinearLayout roundLL;
	Button cancelActivity;
	TextView delayTimer;
	SharePrefManager SM;
	MyCounter timerClock;
	long time;

	protected void onCreate(Bundle savedInstanceState) {
		getSupportActionBar().setBackgroundDrawable(null);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		super.onCreate(savedInstanceState);
		Bundle bundle = getIntent().getExtras();
		long time = bundle.getLong("timer");
		setContentView(R.layout.confirm_sms);
		SM = new SharePrefManager(getApplicationContext());
		cancelActivity = (Button) findViewById(R.id.cancelActivity);
		Utils.TypeFaceButton(cancelActivity, getAssets());
		delayTimer = (TextView) findViewById(R.id.delayTimer);
		timerClock = new MyCounter(time, 1);

		
		timerClock.start();

		cancelActivity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (timerClock != null) {
					timerClock.cancel();
				}
				finish();
			}
		});

	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		if (timerClock != null) {
			timerClock.cancel();
		}	
		super.onStop();
	}



	public class MyCounter extends CountDownTimer {

		public MyCounter(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			new SmsController(getApplicationContext()).send();
			Toast.makeText(getApplicationContext(), "Timer finish",
					Toast.LENGTH_SHORT).show();
			finish();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			delayTimer
					.setText(String.valueOf((millisUntilFinished / 1000) + 1));
		}

	}

}
