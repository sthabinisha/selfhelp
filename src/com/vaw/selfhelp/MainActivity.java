package com.vaw.selfhelp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.github.espiandev.showcaseview.ShowcaseView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.vaw.preferences.SharePrefManager;

public class MainActivity extends SherlockActivity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	Button reportVAW;
	LinearLayout phoneCall, smsQuick, safetyTips;
	ImageView settingsButton;
	TextView self, help;
	SharePrefManager SM;
	ShowcaseView sv;

	// FOR LOCATION MANAGEMENT
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	LocationClient locationClient;
	Location location;

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		locationClient = new LocationClient(this, this, this);
		locationClient.connect();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		locationClient.disconnect();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setBackgroundDrawable(null);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		SM = new SharePrefManager(getApplicationContext());

		if (SM.getState() == 1) {
			Intent intent = new Intent(this, Receiver.class);
			startService(intent);
			SM.setState(1);
		}

		boolean iscon = serviceConnected();
		Toast.makeText(getApplicationContext(), String.valueOf(iscon), 1000)
				.show();

		phoneCall = (LinearLayout) findViewById(R.id.phoneCall);
		smsQuick = (LinearLayout) findViewById(R.id.smsQuick);
		safetyTips = (LinearLayout) findViewById(R.id.safetyTips);
		self = (TextView) findViewById(R.id.selfText);
		help = (TextView) findViewById(R.id.help);

		// Utils.TypeFaceOne(self, getAssets());
		// Utils.TypeFaceOne(help, getAssets());

		reportVAW = (Button) findViewById(R.id.reportVAW);
		// Utils.TypeFaceButton(reportVAW, getAssets());

		reportVAW.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent report = new Intent(MainActivity.this, ReportVAW.class);
				startActivity(report);

			}
		});

		phoneCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, PhoneActivity.class);
				startActivity(i);
			}
		});

		smsQuick.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, SmsActivity.class);
				startActivity(i);
			}
		});

		safetyTips.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this, TipsSlider.class);
				startActivity(i);
			}
		});

	}

	private boolean serviceConnected() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (ConnectionResult.SUCCESS == resultCode) {
			return true;
		} else {
			return false;
		}
	}

	public void getLocationCod() {
		location = locationClient.getLastLocation();
		String loc = String.valueOf(location);
		Toast.makeText(getApplicationContext(), loc, Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();

		inflater.inflate(R.menu.main, menu);
		if (!SM.isvalueavailiable()) {
			ShowcaseView.ConfigOptions co = new ShowcaseView.ConfigOptions();
			co.hideOnClickOutside = true;
			co.fadeInDuration = 1000;
			co.fadeOutDuration = 1000;
			sv = ShowcaseView
					.insertShowcaseViewWithType(
							ShowcaseView.ITEM_ACTION_ITEM,
							R.id.overflow,
							MainActivity.this,
							"Settings",
							"Please add numbers for panic SMS and emergency call to use all features. \n\n For more details you can find \"About\" section in menu",
							co);

		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.smsSettingsOF:
			Intent set = new Intent(MainActivity.this, SMSOptions.class);
			startActivity(set);
			break;
		case R.id.aboutVAW:
			AlertDialog.Builder alter = new AlertDialog.Builder(
					MainActivity.this);
			alter.setMessage(getResources().getString(R.string.about))
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();
								}
							});
			AlertDialog alert = alter.create();
			alert.show();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("DESTROY", "DESTROY WAS CALLED");
		// Intent service = new Intent(MainActivity.this, Receiver.class);
		// SM.setState(0);
		// stopService(service);
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// TODO Auto-generated method stub
		if (connectionResult.hasResolution()) {
			try {
				connectionResult.startResolutionForResult(this,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
			} catch (IntentSender.SendIntentException e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_LONG)
					.show();
		}
	}

	@Override
	public void onConnected(Bundle bundle) {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "CONNTECTED", Toast.LENGTH_LONG)
				.show();
		getLocationCod();
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "DISCONNECTED",
				Toast.LENGTH_LONG).show();
	}

}
