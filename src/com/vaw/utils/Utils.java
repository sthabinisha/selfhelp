package com.vaw.utils;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Utils {
	
	 public static Typeface fontsStyle;

	    public static void TypeFaceOne(TextView tv, AssetManager asm){

	        fontsStyle=Typeface.createFromAsset(asm, "fonts/Roboto-Bold.ttf"); 
	        tv.setTypeface(fontsStyle);
	    }
	    
	    public static void TypeFaceButton(Button btn, AssetManager asm){
	    	fontsStyle=Typeface.createFromAsset(asm, "fonts/Roboto-Light.ttf");
	    	btn.setTypeface(fontsStyle);
	    }
	    
	    public static void TypeFaceLight(TextView tv, AssetManager asm){
	    	fontsStyle=Typeface.createFromAsset(asm, "fonts/Roboto-Light.ttf");
	    	tv.setTypeface(fontsStyle);	    	
	    }
	    
	    public static void TypeFaceReport(TextView tv, AssetManager asm){
	    	fontsStyle=Typeface.createFromAsset(asm, "fonts/RobotoCondensed-Bold.ttf");
	    	tv.setTypeface(fontsStyle);
	    }
	    
	    public static void TypeFaceET(EditText et, AssetManager asm){
	    	fontsStyle=Typeface.createFromAsset(asm, "fonts/Roboto-Black.ttf");
	    	et.setTypeface(fontsStyle);
	    }
	    
}
