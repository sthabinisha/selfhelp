package com.vaw.custom_dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.vaw.selfhelp.R;
import com.vaw.selfhelp.ReportSubmission;

public class CustomDialog extends Dialog implements android.view.View.OnClickListener{
	
	  public Activity c;
	  public Dialog d;
	  public Button cancelButton;
	  public LinearLayout smsButton, emailButton;

	  public CustomDialog(Activity a) {
	    super(a);
	    // TODO Auto-generated constructor stub
	    this.c = a;
	  }

	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    setContentView(R.layout.custom_dialog);
	    smsButton = (LinearLayout)findViewById(R.id.smsButton);
	    emailButton = (LinearLayout)findViewById(R.id.emailButton);
	    cancelButton = (Button)findViewById(R.id.cancelButton);
	    smsButton.setOnClickListener(this);
	    emailButton.setOnClickListener(this);
	    cancelButton.setOnClickListener(this);
	    
	  }

	  @Override
	  public void onClick(View v) {
	    switch (v.getId()) {
	    case R.id.smsButton:
	    	Intent reporting = new Intent(getContext(), ReportSubmission.class);
	    	getContext().startActivity(reporting);
//	    	Toast.makeText(getContext(), "SMS", Toast.LENGTH_SHORT).show();
	    	break;
	    case R.id.emailButton:
	    	Intent gmail = new Intent(Intent.ACTION_VIEW);
            gmail.setClassName("com.google.android.gm","com.google.android.gm.ComposeActivityGmail");
            gmail.putExtra(Intent.EXTRA_EMAIL, new String[] { "rakeeb.rajbhandari@gmail.com" });
            gmail.putExtra(Intent.EXTRA_SUBJECT, "VAW Hack");
            gmail.setType("plain/text");
            getContext().startActivity(gmail);
	    	break;
	    case R.id.cancelButton:
	    	dismiss();
	    	break;
	    default:
	    break;
	    }
	    dismiss();
	  }

}
