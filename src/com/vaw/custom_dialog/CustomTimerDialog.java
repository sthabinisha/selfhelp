package com.vaw.custom_dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.vaw.preferences.SharePrefManager;
import com.vaw.selfhelp.R;

public class CustomTimerDialog extends Dialog implements android.view.View.OnClickListener{

	public Activity c;
	  public Dialog d;
	  public Button cancelButton;
	  TextView timer;
	  SharePrefManager SM;
	 
	  public CustomTimerDialog(Activity a) {
	    super(a);
	    // TODO Auto-generated constructor stub
	    this.c = a;
	  }

	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    SM = new SharePrefManager(getContext());
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    setContentView(R.layout.countdown_dialog);
	    cancelButton = (Button)findViewById(R.id.cancelButtonTimer);
	    timer = (TextView)findViewById(R.id.timer);
	    final MyCounter timerClock = new MyCounter(SM.getTimer(), 100); 
	    timerClock.start();
	    cancelButton.setOnClickListener(this);
	  }

	  @Override
	  public void onClick(View v) {
	    switch (v.getId()) {
	   case R.id.cancelButtonTimer:
	    	dismiss();
	    	break;
	    default:
	    break;
	    }
	    dismiss();
	  }
	  
	  public class MyCounter extends CountDownTimer
	     {
	          
	            public MyCounter(long millisInFuture, long countDownInterval)
	            {
	                super(millisInFuture, countDownInterval);
	            }
	      
	            @Override
	            public void onFinish()
	            {
	                Log.i("Main","Timer Completed");
	                Toast.makeText(getContext(), "Timer finish", Toast.LENGTH_SHORT).show();
	                dismiss();
	            }
	      
	            @Override
	            public void onTick(long millisUntilFinished)
	            {
	                timer.setText(String.valueOf((millisUntilFinished/1000)+1));
	            }
	          
	     }
	
}
